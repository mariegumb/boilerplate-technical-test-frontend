import Vue from "vue";
import Buefy from "buefy";
import "buefy/dist/buefy.css";

import App from "./App.vue";
import VueRouter from 'vue-router';
import router from './router'
import "@fortawesome/fontawesome-free/css/all.css"

Vue.use(VueRouter)

Vue.use(Buefy);
Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
