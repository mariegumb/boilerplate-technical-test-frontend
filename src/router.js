import VueRouter from 'vue-router';
import Home from './views/Home.vue'
import NewUser from './views/NewUser.vue'
import EditUser from './views/EditUser.vue'
import Historique from './views/Historique.vue'

const routes = [
  { path: '/', component: Home },
  { path: '/NewUser', component: NewUser },
  { path: '/EditUser/:id', component: EditUser },
  { path: '/Historique', component: Historique },
]

const router = new VueRouter({
  routes 
})

export default router